def house_of_cards(floors):
    if floors <= 0:
        raise Exception('invalid floors')
    elif floors == 1:
        return 7
    else:
        return floors + (floors + 1) * 2 + house_of_cards(floors - 1)


print(house_of_cards(1))
print(house_of_cards(2))
print(house_of_cards(3))
print(house_of_cards(4))
print(house_of_cards(5))